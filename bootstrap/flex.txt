Bootstrap 4 Flex
Use flex classes to control the layout of Bootstrap 4 components.

Flexbox
The biggest difference between Bootstrap 3 and Bootstrap 4 is that Bootstrap 4 now uses flexbox, instead of floats, to handle the layout.



1. put the items on the left in order.

2.  put items to the right, starting with 3.

3. Put the items with a space between each item.

4. change the flex row to a flex column.

5. put the items at the bottom of the border.

6. name a number of items with numbers and put them right in the border with a reverse class.

7. put the items on the left in the border and use 'order' to determine the order yourself.



